RoboCleaner
===========

A simple Java app based on Spring Boot that simulates a cleaning machine.

###Prerequisites
To build and run this application, you need Git, Java 8 and Maven 3.

###Building the app
Clone this repository by running `git clone https://bitbucket.org/qzagarese/robo-cleaner.git`

Enter the `robocleaner` directory and run the `mvn package` command.

This will create a `target` directory containing a jar file named `robocleaner-0.0.1-SNAPSHOT.jar` 

###Running the app
To execute the application, run the following command from the `robocleaner` directory:

`java -jar target/robocleaner-0.0.1-SNAPSHOT.jar`

This will run the app with an example `input.txt` file.
To run the application with a different input, provide the path to the input file as a command line argument.
The following command, for instance, will run the app with one of the test input files.

` java -jar target/robocleaner-0.0.1-SNAPSHOT.jar src/test/resources/org/example/robocleaner/input-clean-around.txt`

###Running tests
To execute the RoboCleaner tests, simply run the `mvn test` command from the `robocleaner` directory.

##That's all folks! Happy cleaning! 