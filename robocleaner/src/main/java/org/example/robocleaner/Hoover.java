package org.example.robocleaner;

import org.example.robocleaner.command.Command;
import org.example.robocleaner.command.CommandExecutor;
import org.example.robocleaner.util.Pair;


/**
 * A simple hoover that can execute directional commands
 * 
 * 
 * @author bluesoul
 *
 */
public class Hoover implements CommandExecutor<Pair<XYPair, Room>, MoveResult> {

	private int cleanedPatches = 0;

	private XYPair currentPosition;

	private Room room;

	public Hoover(XYPair initialPosition, Room room) {
		this.currentPosition = initialPosition;
		this.room = room;
		if(room.get(currentPosition).isPresent()) {
			cleanedPatches++;
		}
	}

	@Override
	public MoveResult executeCommand(
			Command<Pair<XYPair, Room>, MoveResult> command) {
		MoveResult result = command.execute(new Pair<XYPair, Room>(
				currentPosition, room));
		currentPosition = result.getMoveDestination();
		if (result.isPatchFound()) {
			cleanedPatches++;
		}
		return result;
	}

	public int getCleanedPatches() {
		return cleanedPatches;
	}

	public XYPair getCurrentPosition() {
		return currentPosition;
	}

}
