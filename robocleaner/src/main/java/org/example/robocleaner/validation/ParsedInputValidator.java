package org.example.robocleaner.validation;

import java.util.List;

import org.example.robocleaner.Room;
import org.example.robocleaner.XYPair;
import org.example.robocleaner.command.Command;
import org.example.robocleaner.parsing.ParsedInput;
import org.springframework.stereotype.Component;

/**
 * 
 * A simple validator for a parsed input that check room size as well as hoover
 * and patches positions
 * 
 * @author bluesoul
 *
 * @param <CP> Command parameter type
 * @param <CR> Command result type
 */
@Component
public class ParsedInputValidator<CP, CR> implements
		InputValidator<ParsedInput<CP, CR>> {

	private final String HOOVER_POSITION_ERROR = "Cannot place the cleaner outside the room";

	private final String PATCH_POSITION_ERROR = "Cannot place a patch outside the room";

	@Override
	public void validate(ParsedInput<CP, CR> input) throws ValidationException {
		if(input.getRoomSize() == null || input.getHooverStartPosition() == null) {
			throw new ValidationException("Must provide room size and hoover start position.");
		}
		Room room = checkRoomSize(input.getRoomSize());
		checkInsideRoom(input.getHooverStartPosition(), room,
				HOOVER_POSITION_ERROR);
		for (XYPair patch : input.getPatchesCoordinates()) {
			checkInsideRoom(patch, room, PATCH_POSITION_ERROR);
		}
		checkCommandsExist(input.getCommands());
	}

	private Room checkRoomSize(XYPair roomSize) throws ValidationException {
		Room room = null;
		if (roomSize.getX() > 0 && roomSize.getY() > 0) {
			room = new Room(roomSize);
		} else {
			throw new ValidationException(
					"Invalid parameters for room size. Expecting two positive integers.");
		}
		return room;
	}

	private void checkInsideRoom(XYPair hooverStartPosition, Room room,
			String error) throws ValidationException {
		if (!room.isInside(hooverStartPosition)) {
			throw new ValidationException(error);
		}
	}

	private void checkCommandsExist(List<Command<CP, CR>> commands) throws ValidationException {
		if(commands == null || commands.size() == 0) {
			throw new ValidationException("At least on command must be provided.");
		}
	}

}
