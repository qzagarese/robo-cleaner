package org.example.robocleaner.validation;

public class ValidationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3352251214474062164L;

	public ValidationException(String message) {
		super(message);
	}
	
}
