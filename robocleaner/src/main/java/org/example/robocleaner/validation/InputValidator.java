package org.example.robocleaner.validation;

/**
 * 
 * A simple validation component.
 * 
 * @author bluesoul
 *
 * @param <I>
 */
public interface InputValidator<I> {

	void validate(I input) throws ValidationException;

}
