package org.example.robocleaner.command;

import org.example.robocleaner.MoveResult;
import org.example.robocleaner.Room;
import org.example.robocleaner.XYPair;
import org.example.robocleaner.util.Pair;
import org.springframework.stereotype.Component;


/**
 * 
 * 
 * Move West
 * 
 * @author bluesoul
 *
 */
@Component("W")
public class MoveWestHooverCommand extends MoveHooverCommand{

	@Override
	public MoveResult execute(Pair<XYPair, Room> coordsRoomPair) {
		XYPair dest = new XYPair(coordsRoomPair.getLeft().getX() - 1, coordsRoomPair.getLeft().getY());
		return doMove(coordsRoomPair.getLeft(), dest, coordsRoomPair.getRight());
	}

}
