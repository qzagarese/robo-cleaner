package org.example.robocleaner.command;

import org.example.robocleaner.MoveResult;
import org.example.robocleaner.Room;
import org.example.robocleaner.XYPair;
import org.example.robocleaner.util.Pair;


/**
 * 
 * An abstract Move Command that actually performs move and cleaning 
 * 
 * @author bluesoul
 *
 */
public abstract class MoveHooverCommand implements Command<Pair<XYPair, Room>, MoveResult> {

	public MoveHooverCommand() {
		super();
	}
	
	protected MoveResult doMove(XYPair src, XYPair dest, Room room) {
		MoveResult result = new MoveResult();
		result.setMoveDestination(room.isInside(dest)? dest : src );
		result.setPatchFound(room.get(result.getMoveDestination()).isPresent());
		return result;
	}

}