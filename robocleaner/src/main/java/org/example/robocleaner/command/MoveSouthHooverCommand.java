package org.example.robocleaner.command;

import org.example.robocleaner.MoveResult;
import org.example.robocleaner.Room;
import org.example.robocleaner.XYPair;
import org.example.robocleaner.util.Pair;
import org.springframework.stereotype.Component;

/**
 * 
 * Move South
 * 
 * @author bluesoul
 *
 */
@Component("S")
public class MoveSouthHooverCommand extends MoveHooverCommand {

	@Override
	public MoveResult execute(Pair<XYPair, Room> coordsRoomPair) {
		XYPair dest = new XYPair(coordsRoomPair.getLeft().getX(), coordsRoomPair.getLeft().getY() - 1);
		return doMove(coordsRoomPair.getLeft(), dest, coordsRoomPair.getRight());
	}

}
