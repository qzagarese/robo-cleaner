package org.example.robocleaner.command;

/**
 * 
 * @author bluesoul
 *
 * @param <P> The parameter type
 * @param <R> The result type 
 */
public interface Command<P, R> {

	R execute(P p);

}
