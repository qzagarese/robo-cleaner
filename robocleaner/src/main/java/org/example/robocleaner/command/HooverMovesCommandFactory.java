package org.example.robocleaner.command;

import org.example.robocleaner.MoveResult;
import org.example.robocleaner.Room;
import org.example.robocleaner.XYPair;
import org.example.robocleaner.util.Pair;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * 
 * A simple spring based factory to instantiate directional commands
 * 
 * @author bluesoul
 *
 */
@Component
public class HooverMovesCommandFactory implements
		CommandFactory<Pair<XYPair, Room>, MoveResult> {

	@Autowired
	private ApplicationContext context;

	@SuppressWarnings("unchecked")
	@Override
	public Command<Pair<XYPair, Room>, MoveResult> create(String command)
			throws CommandNotFoundException {
		Command<Pair<XYPair, Room>, MoveResult> c = null;
		try {
			c = context.getBean(command, Command.class);
		} catch (BeansException e) {
			throw new CommandNotFoundException("Cannot find comand for string "
					+ command);
		}
		return c;
	}

}
