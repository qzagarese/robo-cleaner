package org.example.robocleaner.command;

/**
 * 
 * @author bluesoul
 *
 * @param <P> The parameter type
 * @param <R> The result type
 */
public interface CommandFactory<P, R> {

	Command<P, R> create(String command) throws CommandNotFoundException;
	
}
