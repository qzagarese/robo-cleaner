package org.example.robocleaner.command;

/**
 * 
 * @author bluesoul
 *
 * @param <P> The parameter type 
 * @param <R> The result type 
 */
public interface CommandExecutor<P, R> {

	R executeCommand(Command<P, R> command);

}
