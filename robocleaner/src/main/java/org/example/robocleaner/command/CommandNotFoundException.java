package org.example.robocleaner.command;

public class CommandNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7836512445090680224L;

	public CommandNotFoundException(String message) {
		super(message);
	}
	
}
