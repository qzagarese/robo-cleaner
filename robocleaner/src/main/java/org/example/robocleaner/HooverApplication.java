package org.example.robocleaner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.example.robocleaner.execution.CleaningSessionExecutor;
import org.example.robocleaner.parsing.InputParser;
import org.example.robocleaner.parsing.ParseException;
import org.example.robocleaner.parsing.ParsedInput;
import org.example.robocleaner.util.Pair;
import org.example.robocleaner.validation.InputValidator;
import org.example.robocleaner.validation.ValidationException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class HooverApplication extends SpringApplication {

	private final static Logger LOGGER = Logger
			.getLogger(HooverApplication.class.getCanonicalName());

	private static final int WRONG_USAGE_EXIT_STATUS = 64;

	private static final int DATA_FORMAT_ERROR_EXIT_STATUS = 65;

	private static final int NO_INPUT_ERROR_EXIT_STATUS = 66;

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(
				HooverApplication.class, args);
		
		FileInputStream fis = openInput(args);

		ParsedInput<Pair<XYPair, Room>, MoveResult> parsedInput = parseInput(
				context, fis);

		closeInput(fis);

		validateInput(context, parsedInput);

		Hoover hoover = doCleaning(context, parsedInput);

		printOutput(hoover);

		System.exit(0);
		
	}

	/**
	 * 
	 * Provides a stream to read the input file
	 * 
	 * @param args
	 * @return
	 */
	private static FileInputStream openInput(String[] args) {
		File input = null;
	
		if (args.length == 0) {
			input = new File("input.txt");
		} else {
			input = new File(args[0]);
		}
	
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(input);
		} catch (FileNotFoundException e) {
			LOGGER.log(Level.SEVERE,
					"No input.txt file found in the current directory.");
			LOGGER.log(
					Level.SEVERE,
					"Please select a directory containing such a file or provide the path to the input file.");
			LOGGER.log(Level.SEVERE,
					"Usage: java -jar robocleaner.jar /path/to/input.txt");
	
			System.exit(NO_INPUT_ERROR_EXIT_STATUS);
		}
		return fis;
	}

	/**
	 * Creates an executable representation of the input file
	 * 
	 * @param context
	 * @param fis
	 * @return
	 */
	private static ParsedInput<Pair<XYPair, Room>, MoveResult> parseInput(
			ConfigurableApplicationContext context, FileInputStream fis) {
		@SuppressWarnings("unchecked")
		InputParser<Pair<XYPair, Room>, MoveResult> parser = context
				.getBean(InputParser.class);
	
		ParsedInput<Pair<XYPair, Room>, MoveResult> parsedInput = null;
		try {
			parsedInput = parser.parse(fis);
		} catch (ParseException pe) {
			LOGGER.log(Level.SEVERE,
					"Invalid input provided. " + pe.getMessage());
			System.exit(DATA_FORMAT_ERROR_EXIT_STATUS);
		}
		return parsedInput;
	}

	/**
	 * 
	 * Closes the stream to the input file
	 * 
	 * @param fis
	 */
	private static void closeInput(FileInputStream fis) {
		try {
			fis.close();
		} catch (IOException e) {
			LOGGER.log(Level.WARNING,
					"Could not close the input stream for the following reason: "
							+ e.getMessage());
		}
	}

	/**
	 * 
	 * Checks the parsed input is valid in order to execute the cleaning session
	 * 
	 * 
	 * @param context
	 * @param parsedInput
	 */
	private static void validateInput(ConfigurableApplicationContext context,
			ParsedInput<Pair<XYPair, Room>, MoveResult> parsedInput) {
		@SuppressWarnings("unchecked")
		InputValidator<ParsedInput<Pair<XYPair, Room>, MoveResult>> validator = context
				.getBean(InputValidator.class);
		try {
			validator.validate(parsedInput);
		} catch (ValidationException ve) {
			LOGGER.log(Level.SEVERE,
					"Invalid input provided. " + ve.getMessage());
			System.exit(WRONG_USAGE_EXIT_STATUS);
		}
	}

	/**
	 * Executes the cleaning session based on the provided configuration
	 * 
	 * @param context
	 * @param parsedInput
	 * @return
	 */
	private static Hoover doCleaning(ConfigurableApplicationContext context,
			ParsedInput<Pair<XYPair, Room>, MoveResult> parsedInput) {
		@SuppressWarnings("unchecked")
		CleaningSessionExecutor<Hoover, ParsedInput<Pair<XYPair, Room>, MoveResult>> executor = context
				.getBean(CleaningSessionExecutor.class);
	
		Hoover hoover = executor.executeCleaning(parsedInput);
		return hoover;
	}

	/**
	 * Prints the final hoover position and the number of cleaned patches
	 * 
	 * 
	 * @param hoover
	 */
	private static void printOutput(Hoover hoover) {
		System.out.println(hoover.getCurrentPosition().getX() + " "
				+ hoover.getCurrentPosition().getY());
		System.out.println(hoover.getCleanedPatches());
	}

}
