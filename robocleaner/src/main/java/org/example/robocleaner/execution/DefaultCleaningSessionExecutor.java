package org.example.robocleaner.execution;

import org.example.robocleaner.Hoover;
import org.example.robocleaner.MoveResult;
import org.example.robocleaner.Room;
import org.example.robocleaner.XYPair;
import org.example.robocleaner.command.CommandExecutor;
import org.example.robocleaner.parsing.ParsedInput;
import org.example.robocleaner.util.Pair;
import org.springframework.stereotype.Component;

/**
 * A Hoover Cleaning session executor that instantiate a Hoover and a Room
 * according to the parsed input. In this case, the command executor is also 
 * the result of the cleaning session.
 * 
 * 
 * @author bluesoul
 *
 */
@Component
public class DefaultCleaningSessionExecutor extends
		AbstractCleaningSessionExecutor<Hoover, Pair<XYPair, Room>, MoveResult> {

	private Hoover result;
	
	@Override
	protected CommandExecutor<Pair<XYPair, Room>, MoveResult> getCommandExecutor(
			ParsedInput<Pair<XYPair, Room>, MoveResult> parsedInput) {
		Room room = new Room(parsedInput.getRoomSize());
		for (XYPair coords : parsedInput.getPatchesCoordinates()) {
			room.addPatch(coords);
		}
		result = new Hoover(parsedInput.getHooverStartPosition(), room); 
		return result;

	}

	@Override
	protected Hoover getResult() {
		return result;
	}

}
