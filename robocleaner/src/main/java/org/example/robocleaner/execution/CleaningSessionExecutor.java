package org.example.robocleaner.execution;

/**
 * 
 * @author bluesoul
 *
 * @param <ER> Execution Result Type
 * @param <PI> Parsed Input Type
 */
public interface CleaningSessionExecutor <ER, PI> {

	ER executeCleaning(PI parsedInput);
	
}
