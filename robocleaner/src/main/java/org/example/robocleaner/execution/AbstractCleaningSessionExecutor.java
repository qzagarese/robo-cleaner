package org.example.robocleaner.execution;

import org.example.robocleaner.command.Command;
import org.example.robocleaner.command.CommandExecutor;
import org.example.robocleaner.parsing.ParsedInput;


/**
 * 
 * A generic cleaning session executor that gets executable commands from a parsed input. 
 * 
 * @author bluesoul
 *
 * @param <P> The command parameter type
 * @param <R> The command result type
 * @param <ER> The cleaning execution result type
 */
public abstract class AbstractCleaningSessionExecutor<ER, P, R> implements
		CleaningSessionExecutor<ER, ParsedInput<P, R>> {

	@Override
	public ER executeCleaning(ParsedInput<P, R> parsedInput) {
		CommandExecutor<P, R> commandExecutor = getCommandExecutor(parsedInput);
		for (Command<P, R> command : parsedInput.getCommands()) {
			commandExecutor.executeCommand(command);
		}
		return getResult();
	}

	protected abstract CommandExecutor<P, R> getCommandExecutor(ParsedInput<P, R> parsedInput);
	
	protected abstract ER getResult();
	
}
