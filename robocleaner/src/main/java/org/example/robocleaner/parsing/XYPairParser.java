package org.example.robocleaner.parsing;

import org.example.robocleaner.XYPair;

/**
 * A component for creating XYPair instances, starting from an input string
 * 
 * 
 * @author bluesoul
 *
 */
public interface XYPairParser {

	XYPair extractPair(String input) throws ParseException;
	
}
