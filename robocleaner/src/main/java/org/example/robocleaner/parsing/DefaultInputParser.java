package org.example.robocleaner.parsing;

import java.util.ArrayList;
import java.util.List;

import org.example.robocleaner.MoveResult;
import org.example.robocleaner.Room;
import org.example.robocleaner.XYPair;
import org.example.robocleaner.command.Command;
import org.example.robocleaner.command.CommandFactory;
import org.example.robocleaner.command.CommandNotFoundException;
import org.example.robocleaner.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 
 * An input parser that is able, by means of a command factory, to instantiate
 * direction commands for the hoover
 * 
 * @author bluesoul
 *
 */
@Component
public class DefaultInputParser extends
		AbstractInputParser<Pair<XYPair, Room>, MoveResult> {

	@Autowired
	private CommandFactory<Pair<XYPair, Room>, MoveResult> commandFactory;

	@Override
	public CommandFactory<Pair<XYPair, Room>, MoveResult> getCommandFactory() {
		return commandFactory;
	}

	protected List<Command<Pair<XYPair, Room>, MoveResult>> parseCommands(
			String line) throws ParseException {
		String[] commands = getCommandsLineSplitter().split(line);
		List<Command<Pair<XYPair, Room>, MoveResult>> executableCommands = new ArrayList<>();
		for (String command : commands) {
			try {
				Command<Pair<XYPair, Room>, MoveResult> c = commandFactory
						.create(command);
				executableCommands.add(c);
			} catch (CommandNotFoundException e) {
				throw new ParseException("Unrecognized command " + command);
			}
		}
		return executableCommands;
	}

}
