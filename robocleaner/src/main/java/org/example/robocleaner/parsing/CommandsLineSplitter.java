package org.example.robocleaner.parsing;

/**
 * 
 * A splitter that returns a list of command strings, starting from a
 * single line containing all commands
 * 
 * @author bluesoul
 *
 */
public interface CommandsLineSplitter {

	String[] split(String line);

}
