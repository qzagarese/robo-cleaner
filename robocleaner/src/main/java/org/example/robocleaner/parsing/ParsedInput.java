package org.example.robocleaner.parsing;

import java.util.ArrayList;
import java.util.List;

import org.example.robocleaner.XYPair;
import org.example.robocleaner.command.Command;

/**
 * 
 * An executable representation of the input.
 * 
 * @author bluesoul
 *
 * @param <CP> Command parameter type
 * @param <CR> Command result type
 */
public class ParsedInput<CP, CR> {

	private XYPair hooverStartPosition;

	private XYPair roomSize;

	private List<XYPair> patchesCoordinates;

	private List<Command<CP, CR>> commands;

	public List<XYPair> getPatchesCoordinates() {
		return patchesCoordinates;
	}

	public void setPatchesCoordinates(List<XYPair> patchesCoordinates) {
		this.patchesCoordinates = patchesCoordinates;
	}

	public void addPatchCoordinates(XYPair pair) {
		if (patchesCoordinates == null) {
			patchesCoordinates = new ArrayList<XYPair>();
		}
		patchesCoordinates.add(pair);
	}

	public XYPair getRoomSize() {
		return roomSize;
	}

	public void setRoomSize(XYPair roomSize) {
		this.roomSize = roomSize;
	}

	public XYPair getHooverStartPosition() {
		return hooverStartPosition;
	}

	public void setHooverStartPosition(XYPair hooverStartPosition) {
		this.hooverStartPosition = hooverStartPosition;
	}

	public List<Command<CP, CR>> getCommands() {
		return commands;
	}

	public void setCommands(List<Command<CP, CR>> commands) {
		this.commands = commands;
	}

}
