package org.example.robocleaner.parsing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.example.robocleaner.XYPair;
import org.example.robocleaner.command.Command;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * An input file parser that is aware of the file structure, but takes advantage
 * of other components to parse each line.
 * Commands instantiation is not handled at this level.
 * 
 * @author bluesoul
 *
 * @param <CP> The Command parameter type
 * @param <CR> The Command result type
 */
public abstract class AbstractInputParser<CP, CR> implements
		InputParser<CP, CR> {

	@Autowired
	private XYPairParser coordinatesParser;

	@Autowired
	private CommandsLineSplitter splitter;

	@Override
	public ParsedInput<CP, CR> parse(InputStream stream) throws ParseException {
		BufferedReader br = new BufferedReader(new InputStreamReader(stream));

		final ParsedInput<CP, CR> input = new ParsedInput<>();

		String line = null;
		try {
			line = br.readLine();
			while (line != null) {
				processLine(input, line);
				line = br.readLine();
			}
		} catch (IOException e) {
			throw new ParseException("Could not read input file.");
		}
		return input;
	}

	private void processLine(ParsedInput<CP, CR> input, String line)
			throws ParseException {
		if (input.getRoomSize() == null) {
			input.setRoomSize(parseRoomSize(line));
		} else if (input.getHooverStartPosition() == null) {
			input.setHooverStartPosition(parseHooverStartPosition(line));
		} else {
			try {
				input.addPatchCoordinates(parsePatchCoordinates(line));
			} catch (ParseException pe) {
				input.setCommands(parseCommands(line));
			}
		}
	}

	protected CommandsLineSplitter getCommandsLineSplitter() {
		return splitter;
	}

	protected abstract List<Command<CP, CR>> parseCommands(String line)
			throws ParseException;

	private XYPair parsePatchCoordinates(String line) throws ParseException {
		return extractCoordinates(line, "Could not parse patch coordinates.");
	}

	private XYPair parseHooverStartPosition(String line) throws ParseException {
		return extractCoordinates(line,
				"Could not parse hoover start position.");
	}

	private XYPair parseRoomSize(String line) throws ParseException {
		return extractCoordinates(line, "Could not parse room size.");
	}

	private XYPair extractCoordinates(String line, String errorMessage)
			throws ParseException {
		XYPair c = null;
		try {
			c = coordinatesParser.extractPair(line);
		} catch (ParseException pe) {
			throw new ParseException(errorMessage + " " + pe.getMessage());
		}
		return c;
	}

}
