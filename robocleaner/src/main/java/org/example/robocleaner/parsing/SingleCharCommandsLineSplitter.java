package org.example.robocleaner.parsing;

import org.springframework.stereotype.Component;

/**
 * 
 * A simple splitter that assumes that each character in the command line is a command. 
 * 
 * 
 * @author bluesoul
 *
 */
@Component
public class SingleCharCommandsLineSplitter implements CommandsLineSplitter {

	@Override
	public String[] split(String line) {
		return line.trim().split("");
	}

}
