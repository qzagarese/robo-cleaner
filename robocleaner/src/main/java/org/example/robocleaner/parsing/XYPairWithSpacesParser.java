package org.example.robocleaner.parsing;

import org.example.robocleaner.XYPair;
import org.springframework.stereotype.Component;

/**
 * 
 * A simple parser that expects xy pairs with positive values, where x and y are
 * separated by a space.
 * 
 * @author bluesoul
 *
 */
@Component
public class XYPairWithSpacesParser implements XYPairParser {

	@Override
	public XYPair extractPair(String input) throws ParseException {
		String[] tokens = input.trim().split(" ");
		XYPair result = null;

		if (tokens.length == 2) {
			int x, y;
			try {
				x = Integer.parseInt(tokens[0]);
				y = Integer.parseInt(tokens[1]);
			} catch (NumberFormatException e) {
				throw new ParseException("Invalid parameters " + tokens[0]
						+ " " + tokens[1]);
			}

			result = new XYPair(x, y);
		} else {
			throw new ParseException(
					"Expecting two non negative integer values.");
		}
		return result;
	}

}
