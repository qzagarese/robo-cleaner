package org.example.robocleaner.parsing;

public class ParseException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6501601405447871823L;

	public ParseException(String message) {
		super(message);
	}
	
}
