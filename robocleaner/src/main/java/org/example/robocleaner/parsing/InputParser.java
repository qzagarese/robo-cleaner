package org.example.robocleaner.parsing;

import java.io.InputStream;

import org.example.robocleaner.command.CommandFactory;

/**
 * The input parser interface. It parses the room size, the hoover and the patches positions. 
 * It creates, by means of a CommandFactory, a set of executable commands, based on the input file.
 * 
 * @author bluesoul
 *
 * @param <CP> The Command parameter type
 * @param <CR> The Command result type
 */

public interface InputParser<CP, CR> {

	
	ParsedInput<CP, CR> parse(InputStream stream) throws ParseException;
	
	CommandFactory<CP, CR> getCommandFactory();
	
}
