package org.example.robocleaner;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * A simple room representation
 * 
 * @author bluesoul
 *
 */
public class Room {

	private XYPair size;

	private Map<String, XYPair> patches = new HashMap<String, XYPair>();

	public Room(XYPair size) {
		this.size = size;
	}

	public XYPair getSize() {
		return size;
	}

	public void setSize(XYPair size) {
		this.size = size;
	}

	public void addPatch(XYPair coords) {
		if(isInside(coords)) {
			patches.put(coords.toString(), coords);
		}
	}

	public boolean isInside(XYPair coords) {
		return (coords.getX() >= 0) && (coords.getX() < size.getX())
				&& (coords.getY() >= 0) && (coords.getY() < size.getY());
	}

	
	public Optional<XYPair> get(XYPair coords) {
		return Optional.ofNullable(patches.remove(coords.toString()));
	} 
	
}
