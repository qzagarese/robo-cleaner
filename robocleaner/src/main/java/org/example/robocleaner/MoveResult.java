package org.example.robocleaner;

/**
 * The result coming from an execution of a directional command
 * 
 * @author bluesoul
 *
 */
public class MoveResult {

	private XYPair moveDestination;

	private boolean patchFound;

	public XYPair getMoveDestination() {
		return moveDestination;
	}

	public void setMoveDestination(XYPair moveDestination) {
		this.moveDestination = moveDestination;
	}

	public boolean isPatchFound() {
		return patchFound;
	}

	public void setPatchFound(boolean patchFound) {
		this.patchFound = patchFound;
	}

}
