package org.example.robocleaner;

import org.example.robocleaner.parsing.InputParser;
import org.example.robocleaner.parsing.ParseException;
import org.example.robocleaner.parsing.ParsedInput;
import org.example.robocleaner.util.Pair;
import org.example.robocleaner.validation.InputValidator;
import org.example.robocleaner.validation.ValidationException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = HooverApplication.class)
public class ParsingAndValidationTest {

	@Autowired
	private InputParser<Pair<XYPair, Room>, MoveResult> parser;

	@Autowired
	private InputValidator<ParsedInput<Pair<XYPair, Room>, MoveResult>> validator;

	@Test
	public void contextLoads() {
	}

	@Test
	public void testParseExampleFile() throws ParseException {
		parser.parse(this.getClass().getResourceAsStream("input.txt"));
	}

	@Test(expected = ParseException.class)
	public void testUnparsableInput() throws ParseException {
		parser.parse(this.getClass()
				.getResourceAsStream("input-unparsable.txt"));
	}

	@Test(expected = ValidationException.class)
	public void testNoRoomSize() throws ParseException, ValidationException {
		parseAndValidate("input-no-room-size.txt");
	}
	
	
	@Test(expected = ParseException.class)
	public void testNoHooverPosition() throws ParseException, ValidationException {
		parseAndValidate("input-no-hoover-position.txt");
	}
	
	@Test(expected = ValidationException.class)
	public void testNoInstructions() throws ParseException, ValidationException {
		parseAndValidate("input-no-instructions.txt");
	}

	@Test(expected = ValidationException.class)
	public void testWrongRoomSize() throws ParseException, ValidationException {
		parseAndValidate("input-wrong-room-size.txt");
	}

	@Test(expected = ValidationException.class)
	public void testHooverOutsideRoom() throws ParseException, ValidationException {
		parseAndValidate("input-hoover-outside-room.txt");
	}
	
	@Test(expected = ValidationException.class)
	public void testPatchOutsideRoom() throws ParseException, ValidationException {
		parseAndValidate("input-patch-outside-room.txt");
	}
	
	
	private void parseAndValidate(String resource) throws ParseException, ValidationException {
		ParsedInput<Pair<XYPair, Room>, MoveResult> parsedInput = parser
				.parse(this.getClass().getResourceAsStream(
						resource));
		validator.validate(parsedInput);
	}
	
}
