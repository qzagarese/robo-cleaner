package org.example.robocleaner;

import static org.junit.Assert.assertEquals;

import org.example.robocleaner.execution.CleaningSessionExecutor;
import org.example.robocleaner.parsing.InputParser;
import org.example.robocleaner.parsing.ParseException;
import org.example.robocleaner.parsing.ParsedInput;
import org.example.robocleaner.util.Pair;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = HooverApplication.class)
public class CleaningSessionExecutionTest {

	@Autowired
	private InputParser<Pair<XYPair, Room>, MoveResult> parser;

	@Autowired
	private CleaningSessionExecutor<Hoover, ParsedInput<Pair<XYPair, Room>, MoveResult>> executor;

	@Test
	public void contextLoads() {
	}

	@Test
	public void testExampleInput() throws ParseException {
		Hoover hoover = parseAndExecute("input.txt");
		assertEquals(new XYPair(1, 3), hoover.getCurrentPosition());
		assertEquals(1, hoover.getCleanedPatches());
	}

	@Test
	public void testCleanAroundTheRoom() throws ParseException {
		Hoover hoover = parseAndExecute("input-clean-around.txt");
		assertEquals(new XYPair(0, 0), hoover.getCurrentPosition());
		assertEquals(11, hoover.getCleanedPatches());
	}

	@Test
	public void testStartFromPatch() throws ParseException {
		Hoover hoover = parseAndExecute("input-from-patch.txt");
		assertEquals(new XYPair(1, 0), hoover.getCurrentPosition());
		assertEquals(1, hoover.getCleanedPatches());
	}
	
	@Test
	public void testPassTwiceOnPatchesCoords() throws ParseException {
		Hoover hoover = parseAndExecute("input-twice-on-patches.txt");
		assertEquals(new XYPair(0, 0), hoover.getCurrentPosition());
		assertEquals(11, hoover.getCleanedPatches());
	}
	

	private Hoover parseAndExecute(String resource) throws ParseException {
		ParsedInput<Pair<XYPair, Room>, MoveResult> input = parser.parse(this
				.getClass().getResourceAsStream(resource));
		Hoover hoover = (Hoover) executor.executeCleaning(input);
		return hoover;
	}

}
